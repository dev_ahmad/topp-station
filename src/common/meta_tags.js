const MetaTags = {
	FacebookAds: {
		title: 'Facebook Ads | One app to design, collaborate and scale',
		description:
			'The all-in-one digital design platform that helps you create content, scale your brand, and collaborate with your team like never before. Everything you need to tell your story, in one place.',
		route: '/search/facebook-ads',
		keywords: '',
	},
	Login: {
		title: 'login to live',
		description: 'login to simplified app',
		route: '/login',
		keywords: '',
	},
	InstaStories: {
		title: 'Instagram Stories | One app to design, collaborate and scale',
		description:
			'Design and Collaboration platform for modern marketing teams.From idea to launch in minutes',
		route: '/search/instagram-stories',
		keywords: '',
	},
	Index: {
		title: 'Simplified | One app to design, collaborate and scale',
		description:
			'The all-in-one digital design platform that helps you create content, scale your brand, and collaborate with your team like never before. Everything you need to tell your story, in one place.',
		route: '/',
		keywords: '',
	},
	default: {
		title: 'KC',
		description:
			'The all-in-one digital design platform that helps you create content, scale your brand, and collaborate with your team like never before. Everything you need to tell your story, in one place.',
		route: '/',
		keywords: '',
	},
};

export default MetaTags;
